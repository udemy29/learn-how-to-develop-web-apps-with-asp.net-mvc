﻿Imports System.Data.Entity
Imports System.Net
Imports System.Web.Mvc
Imports AAddressBookApp.Models

Namespace Controllers

    Public Class AddressesController
        Inherits System.Web.Mvc.Controller

        Private ReadOnly db As New AddressBookInfoEntities

        ' GET: Addresses
        Function Index() As ActionResult
            Dim addresses = db.Addresses.Include(Function(a) a.Group).OrderBy(Function(a) a.Kana)
            Return View(addresses.ToList())
        End Function

        ' GET: Addresses/Details/5
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim address As Address = db.Addresses.Find(id)
            If IsNothing(address) Then
                Return HttpNotFound()
            End If
            Return View(address)
        End Function

        ' GET: Addresses/Create
        Function Create() As ActionResult
            ViewBag.GroupId = New SelectList(db.Groups, "GroupId", "GroupName")
            Return View()
        End Function

        ' POST: Addresses/Create
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Create(<Bind(Include:="AddressId,FullName,Kana,ZipCode,PrefectureItem,StreetAddress,Telephone,Mail,GroupId")> ByVal address As Address) As ActionResult
            If ModelState.IsValid Then
                address.Prefecture = address.PrefectureItem.ToString
                address.CreatedBy = 1
                address.CreatedOn = Date.Now
                address.UpdatedBy = 1
                address.UpdatedOn = Date.Now

                db.Addresses.Add(address)
                db.SaveChanges()
                Return RedirectToAction("Search")
            End If
            ViewBag.GroupId = New SelectList(db.Groups, "GroupId", "GroupName", address.GroupId)
            Return View(address)
        End Function

        ' GET: Addresses/Edit/5
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim address As Address = db.Addresses.Find(id)
            If IsNothing(address) Then
                Return HttpNotFound()
            End If

            Dim p As Prefectures
            If [Enum].TryParse(address.Prefecture, p) Then
                address.PrefectureItem = p
            End If

            ViewBag.GroupId = New SelectList(db.Groups, "GroupId", "GroupName", address.GroupId)
            Return View(address)
        End Function

        ' POST: Addresses/Edit/5
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Edit(<Bind(Include:="AddressId,FullName,Kana,ZipCode,PrefectureItem,StreetAddress,Telephone,Mail,GroupId,CreatedBy,CreatedOn")> ByVal address As Address) As ActionResult
            If ModelState.IsValid Then
                address.Prefecture = address.PrefectureItem.ToString
                address.UpdatedBy = 1
                address.UpdatedOn = Date.Now

                db.Entry(address).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Search")
            End If
            ViewBag.GroupId = New SelectList(db.Groups, "GroupId", "GroupName", address.GroupId)
            Return View(address)
        End Function

        ' GET: Addresses/Delete/5
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim address As Address = db.Addresses.Find(id)
            If IsNothing(address) Then
                Return HttpNotFound()
            End If
            Return View(address)
        End Function

        ' POST: Addresses/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim address As Address = db.Addresses.Find(id)
            db.Addresses.Remove(address)
            db.SaveChanges()
            Return RedirectToAction("Search")
        End Function

        Function Search(<Bind(Include:="Kana")> model As SearchViewModel) As ActionResult
            If Not String.IsNullOrEmpty(model.Kana) Then
                Dim list = db.Addresses.Where(Function(item) item.Kana.IndexOf(model.Kana) = 0).ToList()
                model.Addresses = list
            Else
                model.Addresses = db.Addresses.OrderBy(Function(a) a.FullName).ToList()
            End If
            Return View(model)
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

    End Class

End Namespace