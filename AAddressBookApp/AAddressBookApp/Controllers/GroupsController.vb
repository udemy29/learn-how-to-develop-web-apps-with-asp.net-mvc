﻿Imports System.Data.Entity
Imports System.Net
Imports System.Web.Mvc
Imports AAddressBookApp.Models

Namespace Controllers

    Public Class GroupsController
        Inherits System.Web.Mvc.Controller

        Private db As New AddressBookInfoEntities

        ' GET: Groups
        Function Index() As ActionResult
            Return View(db.Groups.ToList())
        End Function

        ' GET: Groups/Details/5
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim group As Group = db.Groups.Find(id)
            If IsNothing(group) Then
                Return HttpNotFound()
            End If
            Return View(group)
        End Function

        ' GET: Groups/Create
        Function Create() As ActionResult
            Return View()
        End Function

        ' POST: Groups/Create
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Create(<Bind(Include:="GroupId,GroupName")> ByVal group As Group) As ActionResult
            If ModelState.IsValid Then
                group.CreatedBy = 1
                group.CreatedOn = Date.Now
                group.UpdatedBy = 1
                group.UpdatedOn = Date.Now

                db.Groups.Add(group)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            Return View(group)
        End Function

        ' GET: Groups/Edit/5
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim group As Group = db.Groups.Find(id)
            If IsNothing(group) Then
                Return HttpNotFound()
            End If
            Return View(group)
        End Function

        ' POST: Groups/Edit/5
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Edit(<Bind(Include:="GroupId,GroupName,CreatedBy,CreatedOn")> ByVal group As Group) As ActionResult
            If ModelState.IsValid Then
                group.UpdatedBy = 1
                group.UpdatedOn = Date.Now

                db.Entry(group).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            Return View(group)
        End Function

        ' GET: Groups/Delete/5
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim group As Group = db.Groups.Find(id)
            If IsNothing(group) Then
                Return HttpNotFound()
            End If
            Return View(group)
        End Function

        ' POST: Groups/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim group As Group = db.Groups.Find(id)
            db.Groups.Remove(group)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

    End Class

End Namespace