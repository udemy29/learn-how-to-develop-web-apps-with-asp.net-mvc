﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Namespace Models

    <MetadataType(GetType(GroupMetadata))>
    Partial Public Class Group

    End Class

    Public Class GroupMetadata

        <Key>
        Public Property GroupId As Integer

        <DisplayName("グループ名")>
        <Required>
        Public Property GroupName As String

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

    End Class

End Namespace