﻿Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations

Namespace Models

    Public Enum Prefectures
        北海道
        青森県
        岩手県
        宮城県
        秋田県
        山形県
        福島県
        東京都
        神奈川県
        埼玉県
        千葉県
        茨城県
        栃木県
        群馬県
        山梨県
        新潟県
        長野県
        富山県
        石川県
        福井県
        愛知県
        岐阜県
        静岡県
        三重県
        大阪府
        兵庫県
        京都府
        滋賀県
        奈良県
        和歌山県
        鳥取県
        島根県
        岡山県
        広島県
        山口県
        徳島県
        香川県
        愛媛県
        高知県
        福岡県
        佐賀県
        長崎県
        熊本県
        大分県
        宮崎県
        鹿児島県
        沖縄県
    End Enum

    <MetadataType(GetType(AddressMetadata))>
    Partial Public Class Address

        <DisplayName("都道府県")>
        Public Property PrefectureItem As Prefectures?

    End Class

    Public Class AddressMetadata

        <Key>
        Public Property AddressId As Integer

        <DisplayName("氏名")>
        <Required>
        Public Property FullName As String

        <DisplayName("カナ")>
        <Required>
        <RegularExpression("[ァ-ヶ]+")>
        Public Property Kana As String

        <DisplayName("郵便番号")>
        <RegularExpression("[0-9]+")>
        <StringLength(7)>
        Public Property ZipCode As String

        <DisplayName("都道府県")>
        Public Property Prefecture As String

        <DisplayName("住所")>
        Public Property StreetAddress As String

        <DisplayName("電話番号")>
        <RegularExpression("[0-9]+")>
        <StringLength(11)>
        Public Property Telephone As String

        <DisplayName("メール")>
        <DataType(DataType.EmailAddress)>
        Public Property Mail As String

        <DisplayName("グループ")>
        Public Property GroupId As Integer?

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

    End Class

End Namespace