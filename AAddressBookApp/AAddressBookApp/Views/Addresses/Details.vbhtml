﻿@ModelType AAddressBookApp.Models.Address
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>詳細</h2>

<div>
    <h4>住所録</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.FullName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FullName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Kana)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Kana)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ZipCode)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ZipCode)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Prefecture)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Prefecture)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.StreetAddress)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.StreetAddress)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Telephone)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Telephone)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Mail)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Mail)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Group.GroupName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Group.GroupName)
        </dd>
    </dl>
</div>
<p>
    @Html.ActionLink("編集", "Edit", New With {.id = Model.AddressId}) |
    @Html.ActionLink("リストへ戻る", "Search")
</p>