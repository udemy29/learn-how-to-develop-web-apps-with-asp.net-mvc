﻿@Imports AAddressBookApp.Models
@ModelType AAddressBookApp.Models.SearchViewModel
@Code
    ViewData("Title") = "住所録一覧"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>Search</h2>

@Using (Html.BeginForm("Search", "Addresses", FormMethod.Get))
    @<div class="form-horizontal">
        <h4>住所録一覧</h4>
        <hr />
        <p>
            @Html.ActionLink("新規作成", "Create")
        </p>
        @Html.ValidationSummary(True, "", New With {.class = "text-danger"})
        <div class="form-group">
            @Html.LabelFor(Function(model) model.Kana, htmlAttributes:=New With {.class = "control-label col-md-2"})
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Kana, New With {.htmlAttributes = New With {.class = "form-control"}})
                @Html.ValidationMessageFor(Function(model) model.Kana, "", New With {.class = "text-danger"})
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="検索" class="btn btn-default" />
            </div>
        </div>
    </div>
End Using

@code
    Dim address As New Address
End Code
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) address.FullName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.Kana)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.ZipCode)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.Prefecture)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.StreetAddress)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.Telephone)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.Mail)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) address.Group.GroupName)
        </th>
        <th></th>
    </tr>

    @For Each item In Model.Addresses
        @<tr>
            <td>
                @Html.DisplayFor(Function(modelItem) item.FullName)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Kana)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.ZipCode)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Prefecture)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.StreetAddress)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Telephone)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Mail)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.Group.GroupName)
            </td>
            <td>
                @Html.ActionLink("編集", "Edit", New With {.id = item.AddressId}) |
                @Html.ActionLink("詳細", "Details", New With {.id = item.AddressId}) |
                @Html.ActionLink("削除", "Delete", New With {.id = item.AddressId})
            </td>
        </tr>
    Next
</table>
<script src="~/Scripts/jquery-3.4.1.min.js"></script>
<script src="~/Scripts/jquery.validate.min.js"></script>
<script src="~/Scripts/jquery.validate.unobtrusive.min.js"></script>