﻿@ModelType IEnumerable(Of AAddressBookApp.Models.Address)
@Code
    ViewData("Title") = "住所録一覧"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>住所録一覧</h2>

<p>
    @Html.ActionLink("新規作成", "Create")
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.FullName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Kana)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.ZipCode)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Prefecture)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.StreetAddress)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Telephone)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Mail)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.Group.GroupName)
        </th>
        <th></th>
    </tr>

@For Each item In Model
    @<tr>
        <td>
            @Html.DisplayFor(Function(modelItem) item.FullName)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Kana)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.ZipCode)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Prefecture)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.StreetAddress)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Telephone)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Mail)
        </td>
        <td>
            @Html.DisplayFor(Function(modelItem) item.Group.GroupName)
        </td>
        <td>
            @Html.ActionLink("編集", "Edit", New With {.id = item.AddressId}) |
            @Html.ActionLink("詳細", "Details", New With {.id = item.AddressId}) |
            @Html.ActionLink("削除", "Delete", New With {.id = item.AddressId})
        </td>
    </tr>
Next

</table>
