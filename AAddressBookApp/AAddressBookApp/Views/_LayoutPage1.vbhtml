﻿<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title>@ViewData("Title")</title>
    <link rel="stylesheet" href="~/Content/bootstrap.css" />
    <style type="text/css">
        body {
            padding-top: 70px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                @Html.ActionLink("AddressBook", "Search", New With {.Controller = "Addresses"}, New With {.class = "navbar-brand"})
            </div>
            <ul class="nav navbar-nav">
                <li>
                    @Html.ActionLink("グループ管理", "Index", New With {.Controller = "Groups"}, Nothing)
                </li>
            </ul>
        </div>
    </nav>
    <div class="container">
        @RenderBody()
    </div>
</body>
</html>