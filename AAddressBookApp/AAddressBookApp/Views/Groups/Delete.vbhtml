﻿@ModelType AAddressBookApp.Models.Group
@Code
    ViewData("Title") = "グループ削除"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>削除</h2>

<h3>このグループを削除してもよろしいですか？</h3>
<div>
    <h4>グループ</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.GroupName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.GroupName)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="削除" class="btn btn-danger" /> |
            @Html.ActionLink("リストへ戻る", "Index")
        </div>
    End Using
</div>
