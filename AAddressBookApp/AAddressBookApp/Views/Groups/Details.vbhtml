﻿@ModelType AAddressBookApp.Models.Group
@Code
    ViewData("Title") = "グループ詳細"
    Layout = "~/Views/_LayoutPage1.vbhtml"
End Code

<h2>詳細</h2>

<div>
    <h4>グループ</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.GroupName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.GroupName)
        </dd>

    </dl>
</div>
<p>
    @Html.ActionLink("編集", "Edit", New With {.id = Model.GroupId}) |
    @Html.ActionLink("リストへ戻る", "Index")
</p>
