Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Models.Entities

    <Table("FREE.Roles")>
    Partial Public Class Role

        Public Sub New()
            Users = New HashSet(Of User)()
        End Sub

        <Key>
        Public Property RoleId As Integer

        <DisplayName("ロール名")>
        <Required>
        <StringLength(30)>
        Public Property RoleName As String

        <DisplayName("有効")>
        Public Property IsActive As Boolean

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

        Public Overridable Property Users As ICollection(Of User)
    End Class

End Namespace