Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Models.Entities

    <Table("FREE.Users")>
    Partial Public Class User

        Public Sub New()
            Todos = New HashSet(Of Todo)()
            Roles = New HashSet(Of Role)()
        End Sub

        <Key>
        Public Property UserId As Integer

        <DisplayName("ユーザ名")>
        <Required>
        <Index(IsUnique:=True)>
        <StringLength(256)>
        Public Property UserName As String

        <DisplayName("パスワード")>
        <Required>
        <DataType(DataType.Password)>
        <StringLength(500)>
        Public Property Password As String

        <DisplayName("姓")>
        <Required>
        <StringLength(30)>
        Public Property FamilyName As String

        <DisplayName("名")>
        <Required>
        <StringLength(30)>
        Public Property GivenName As String

        <DisplayName("有効")>
        Public Property IsActive As Boolean

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成者名")>
        <NotMapped>
        Public Property Creator As String

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新者名")>
        <NotMapped>
        Public Property Updater As String

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

        Public Overridable Property Todos As ICollection(Of Todo)

        Public Overridable Property Roles As ICollection(Of Role)

        <DisplayName("ロール")>
        <NotMapped>
        Public Property RoleIds As List(Of Integer)

    End Class

End Namespace