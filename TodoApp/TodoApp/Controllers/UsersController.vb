﻿Imports System.Data.Entity
Imports System.Net
Imports TodoApp.Models.Dto
Imports TodoApp.Models.Entities
Imports TodoApp.Models.Providers

Namespace Controllers

    <Authorize(Roles:="Administrators")>
    Public Class UsersController
        Inherits Controller

        Private ReadOnly db As New FreeContext

        Private ReadOnly membershipProvider As New CustomMembershipProvider

        ' GET: Users
        Function Index() As ActionResult
            Return View(db.Users.ToList())
        End Function

        ' GET: Users/Details/5
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If

            Dim user As User = db.Users.Find(id)
            user.Creator = db.Users.Find(user.CreatedBy).UserName
            user.Updater = db.Users.Find(user.UpdatedBy).UserName
            If IsNothing(user) Then
                Return HttpNotFound()
            End If

            Return View(user)
        End Function

        ' GET: Users/Create
        Function Create() As ActionResult
            SetRoles(New List(Of Role))
            Return View()
        End Function

        ' POST: Users/Create
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Create(<Bind(Include:="UserId,UserName,Password,RoleIds,FamilyName,GivenName")> ByVal user As User) As ActionResult
            Dim roles = db.Roles.Where(Function(r) user.RoleIds.Contains(r.RoleId)).ToList
            If ModelState.IsValid Then
                user.Roles = roles

                user.Password = membershipProvider.GeneratePasswordHash(user.UserName, user.Password)
                user.IsActive = True
                Dim loginUser = db.Users _
                    .Where(Function(u) u.UserName = Me.User.Identity.Name) _
                    .FirstOrDefault
                user.CreatedBy = loginUser.UserId
                user.CreatedOn = Date.Now
                user.UpdatedBy = loginUser.UserId
                user.UpdatedOn = Date.Now

                db.Users.Add(user)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            SetRoles(roles)
            Return View(user)
        End Function

        ' GET: Users/Edit/5
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim user As User = db.Users _
                .Where(Function(u) u.UserId = id) _
                .Include(Function(u) u.Roles) _
                .FirstOrDefault
            user.Creator = db.Users.Find(user.CreatedBy).UserName
            user.Updater = db.Users.Find(user.UpdatedBy).UserName

            If IsNothing(user) Then
                Return HttpNotFound()
            End If

            Dim model As New UserEditDto With {
                .UserId = user.UserId, .UserName = user.UserName,
                .FamilyName = user.FamilyName, .GivenName = user.GivenName,
                .IsActive = user.IsActive, .CreatedBy = user.CreatedBy, .CreatedOn = user.CreatedOn,
                .UpdatedBy = user.UpdatedBy, .UpdatedOn = user.UpdatedOn
            }

            SetRoles(user.Roles)
            Return View(model)
        End Function

        ' POST: Users/Edit/5
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Edit(<Bind(Include:="UserId,UserName,Password,RoleIds,FamilyName,GivenName,IsActive,CreatedBy,CreatedOn")> ByVal user As UserEditDto) As ActionResult
            Dim roles = db.Roles.Where(Function(r) user.RoleIds.Contains(r.RoleId)).ToList
            If ModelState.IsValid Then
                Dim dbUser = db.Users.Find(user.UserId)
                If dbUser Is Nothing Then
                    Return HttpNotFound()
                End If

                If Not String.IsNullOrEmpty(user.Password) AndAlso Not dbUser.Password.Equals(user.Password) Then
                    dbUser.Password = membershipProvider.GeneratePasswordHash(dbUser.UserName, user.Password)
                End If

                dbUser.FamilyName = user.FamilyName
                dbUser.GivenName = user.GivenName
                dbUser.IsActive = user.IsActive
                dbUser.Roles.Clear()
                For Each role In roles
                    dbUser.Roles.Add(role)
                Next

                Dim loginUser = db.Users _
                    .Where(Function(u) u.UserName = Me.User.Identity.Name) _
                    .FirstOrDefault
                dbUser.UpdatedBy = loginUser.UserId
                dbUser.UpdatedOn = Date.Now

                db.Entry(dbUser).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            SetRoles(roles)
            Return View(user)
        End Function

        ' GET: Users/Delete/5
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim user As User = db.Users.Find(id)
            If IsNothing(user) Then
                Return HttpNotFound()
            End If
            Return View(user)
        End Function

        ' POST: Users/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim user As User = db.Users.Find(id)
            user.Roles.Clear()
            db.Users.Remove(user)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        Private Sub SetRoles(userRoles As ICollection(Of Role))
            Dim roles = userRoles.Select(Function(ur) ur.RoleId).ToArray

            Dim list = db.Roles _
                .Select(Function(r) New SelectListItem With {
                    .Text = r.RoleName,
                    .Value = r.RoleId.ToString,
                    .Selected = roles.Contains(r.RoleId)
                }) _
                .ToList
            ViewBag.RoleIds = list
        End Sub

    End Class

End Namespace