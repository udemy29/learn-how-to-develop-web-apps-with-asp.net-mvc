﻿Imports System.Data.Entity
Imports System.Net
Imports TodoApp.Models.Entities

Namespace Controllers

    <Authorize>
    Public Class TodosController
        Inherits Controller

        Private ReadOnly db As New FreeContext

        ' GET: Todos
        Function Index() As ActionResult
            Dim loginUser = db.Users _
                .Where(Function(u) u.UserName = User.Identity.Name) _
                .Include(Function(u) u.Todos) _
                .FirstOrDefault
            If loginUser IsNot Nothing Then
                Return View(loginUser.Todos)
            End If

            Return View(New List(Of Todo))
        End Function

        ' GET: Todos/Details/5
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim todo As Todo = db.Todos.Find(id)
            todo.Creator = db.Users.Find(todo.CreatedBy).UserName
            todo.Updater = db.Users.Find(todo.UpdatedBy).UserName
            If IsNothing(todo) Then
                Return HttpNotFound()
            End If
            Return View(todo)
        End Function

        ' GET: Todos/Create
        Function Create() As ActionResult
            ViewBag.UserId = New SelectList(db.Users, "UserId", "UserName")
            Return View()
        End Function

        ' POST: Todos/Create
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Create(<Bind(Include:="TodoId,Summary,Detail,DueDate")> ByVal todo As Todo) As ActionResult
            If ModelState.IsValid Then
                Dim loginUser = db.Users _
                    .Where(Function(u) u.UserName = User.Identity.Name) _
                    .FirstOrDefault
                If loginUser IsNot Nothing Then
                    todo.User = loginUser
                    todo.CreatedBy = loginUser.UserId
                    todo.CreatedOn = Date.Now
                    todo.UpdatedBy = loginUser.UserId
                    todo.UpdatedOn = Date.Now

                    db.Todos.Add(todo)
                    db.SaveChanges()
                    Return RedirectToAction("Index")
                End If
            End If
            ViewBag.UserId = New SelectList(db.Users, "UserId", "UserName", todo.UserId)
            Return View(todo)
        End Function

        ' GET: Todos/Edit/5
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim todo As Todo = db.Todos.Find(id)
            todo.Creator = db.Users.Find(todo.CreatedBy).UserName
            todo.Updater = db.Users.Find(todo.UpdatedBy).UserName
            If IsNothing(todo) Then
                Return HttpNotFound()
            End If
            ViewBag.UserId = New SelectList(db.Users, "UserId", "UserName", todo.UserId)
            Return View(todo)
        End Function

        ' POST: Todos/Edit/5
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        Function Edit(<Bind(Include:="TodoId,Summary,Detail,DueDate,IsDone,UserId,CreatedBy,CreatedOn")> ByVal todo As Todo) As ActionResult
            If ModelState.IsValid Then
                Dim loginUser = db.Users _
                    .Where(Function(u) u.UserName = User.Identity.Name) _
                    .FirstOrDefault
                todo.UpdatedBy = loginUser.UserId
                todo.UpdatedOn = Date.Now

                db.Entry(todo).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.UserId = New SelectList(db.Users, "UserId", "UserName", todo.UserId)
            Return View(todo)
        End Function

        ' GET: Todos/Delete/5
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim todo As Todo = db.Todos.Find(id)
            If IsNothing(todo) Then
                Return HttpNotFound()
            End If
            Return View(todo)
        End Function

        ' POST: Todos/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim todo As Todo = db.Todos.Find(id)
            db.Todos.Remove(todo)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

    End Class

End Namespace