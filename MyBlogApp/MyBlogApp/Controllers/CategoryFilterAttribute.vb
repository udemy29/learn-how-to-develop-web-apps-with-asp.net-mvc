﻿Imports System.Data.Entity
Imports MyBlogApp.Models

Namespace Controllers

    <AttributeUsage(AttributeTargets.Class)>
    Public Class CategoryFilterAttribute
        Inherits FilterAttribute
        Implements IActionFilter

        Public Sub OnActionExecuting(filterContext As ActionExecutingContext) Implements IActionFilter.OnActionExecuting

        End Sub

        Public Sub OnActionExecuted(filterContext As ActionExecutedContext) Implements IActionFilter.OnActionExecuted
            Using db As New FreeContext
                Dim categories = db.Categories.Include(Function(c) c.Articles)
                For Each c In categories
                    c.ArticleCount = c.Articles.Count
                Next
                filterContext.Controller.ViewBag.Categories = categories.ToList
            End Using
        End Sub

    End Class

End Namespace