﻿Imports System.Data.Entity
Imports System.Net
Imports MyBlogApp.Models
Imports MyBlogApp.Models.Entities

Namespace Controllers

    <CategoryFilter>
    Public Class CategoriesController
        Inherits Controller

        Private ReadOnly db As New FreeContext

        ' GET: Categories
        <AllowAnonymous>
        Function Index() As ActionResult
            Dim categories = db.Categories.Include(Function(c) c.Articles)
            For Each c In categories
                c.ArticleCount = c.Articles.Count
            Next
            Return View(categories.ToList())
        End Function

        ' GET: Categories/Details/5
        <AllowAnonymous>
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim category As Category = db.Categories.Include(Function(c) c.Articles).Where(Function(c) c.CategoryId = id).FirstOrDefault
            category.ArticleCount = category.Articles.Count
            If IsNothing(category) Then
                Return HttpNotFound()
            End If
            Return View(category)
        End Function

        ' GET: Categories/Create
        <Authorize(Roles:="Owners")>
        Function Create() As ActionResult
            Return View()
        End Function

        ' POST: Categories/Create
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        <Authorize(Roles:="Owners")>
        Function Create(<Bind(Include:="CategoryId,CategoryName")> ByVal category As Category) As ActionResult
            If ModelState.IsValid Then
                category.ArticleCount = 0
                category.CreatedBy = 1
                category.CreatedOn = Date.Now
                category.UpdatedBy = 1
                category.UpdatedOn = Date.Now

                db.Categories.Add(category)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            Return View(category)
        End Function

        ' GET: Categories/Edit/5
        <Authorize(Roles:="Owners")>
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim category As Category = db.Categories.Find(id)
            If IsNothing(category) Then
                Return HttpNotFound()
            End If
            Return View(category)
        End Function

        ' POST: Categories/Edit/5
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        <Authorize(Roles:="Owners")>
        Function Edit(<Bind(Include:="CategoryId,CategoryName,CreatedBy,CreatedOn")> ByVal category As Category) As ActionResult
            If ModelState.IsValid Then
                category.UpdatedBy = 1
                category.UpdatedOn = Date.Now

                db.Entry(category).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            Return View(category)
        End Function

        ' GET: Categories/Delete/5
        <Authorize(Roles:="Owners")>
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim category As Category = db.Categories.Include(Function(c) c.Articles).Where(Function(c) c.CategoryId = id).FirstOrDefault
            If IsNothing(category) Then
                Return HttpNotFound()
            End If

            Dim articleCount As Integer = category.Articles.Count

            If articleCount <> 0 Then
                Return HttpNotFound()
            End If
            category.ArticleCount = category.Articles.Count

            Return View(category)
        End Function

        ' POST: Categories/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        <Authorize(Roles:="Owners")>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim category As Category = db.Categories.Find(id)
            db.Categories.Remove(category)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

    End Class

End Namespace