﻿Imports System.Data.Entity
Imports System.Net
Imports MyBlogApp.Models
Imports MyBlogApp.Models.Entities

Namespace Controllers

    <CategoryFilter>
    Public Class ArticlesController
        Inherits System.Web.Mvc.Controller

        Private ReadOnly db As New FreeContext

        ' GET: Articles
        <AllowAnonymous>
        Function Index() As ActionResult
            Dim articles = db.Articles _
                .Include(Function(a) a.Category) _
                .OrderBy(Function(a) a.Category.CategoryName)
            Return View(articles.ToList())
        End Function

        ' GET: Articles/Details/5
        <AllowAnonymous>
        Function Details(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim article As Article = db.Articles.Find(id)
            If IsNothing(article) Then
                Return HttpNotFound()
            End If
            Return View(article)
        End Function

        ' GET: Articles/Create
        <Authorize(Roles:="Owners")>
        Function Create() As ActionResult
            ViewBag.CategoryId = New SelectList(db.Categories, "CategoryId", "CategoryName")
            Return View()
        End Function

        ' POST: Articles/Create
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        <Authorize(Roles:="Owners")>
        Function Create(<Bind(Include:="ArticleId,Title,Body,CategoryId")> ByVal article As Article) As ActionResult
            If ModelState.IsValid Then
                article.CreatedBy = 1
                article.CreatedOn = Date.Now
                article.UpdatedBy = 1
                article.UpdatedOn = Date.Now

                db.Articles.Add(article)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.CategoryId = New SelectList(db.Categories, "CategoryId", "CategoryName", article.CategoryId)
            Return View(article)
        End Function

        ' GET: Articles/Edit/5
        <Authorize(Roles:="Owners")>
        Function Edit(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim article As Article = db.Articles.Find(id)
            If IsNothing(article) Then
                Return HttpNotFound()
            End If
            ViewBag.CategoryId = New SelectList(db.Categories, "CategoryId", "CategoryName", article.CategoryId)
            Return View(article)
        End Function

        ' POST: Articles/Edit/5
        '過多ポスティング攻撃を防止するには、バインド先とする特定のプロパティを有効にしてください。
        '詳細については、https://go.microsoft.com/fwlink/?LinkId=317598 をご覧ください。
        <HttpPost()>
        <ValidateAntiForgeryToken()>
        <Authorize(Roles:="Owners")>
        Function Edit(<Bind(Include:="ArticleId,Title,Body,CreatedBy,CreatedOn,CategoryId")> ByVal article As Article) As ActionResult
            If ModelState.IsValid Then
                article.UpdatedBy = 1
                article.UpdatedOn = Date.Now

                db.Entry(article).State = EntityState.Modified
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If
            ViewBag.CategoryId = New SelectList(db.Categories, "CategoryId", "CategoryName", article.CategoryId)
            Return View(article)
        End Function

        ' GET: Articles/Delete/5
        <Authorize(Roles:="Owners")>
        Function Delete(ByVal id As Integer?) As ActionResult
            If IsNothing(id) Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If
            Dim article As Article = db.Articles.Find(id)
            If IsNothing(article) Then
                Return HttpNotFound()
            End If
            Return View(article)
        End Function

        ' POST: Articles/Delete/5
        <HttpPost()>
        <ActionName("Delete")>
        <ValidateAntiForgeryToken()>
        <Authorize(Roles:="Owners")>
        Function DeleteConfirmed(ByVal id As Integer) As ActionResult
            Dim article As Article = db.Articles.Find(id)
            db.Articles.Remove(article)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End Function

        <HttpPost>
        <ValidateAntiForgeryToken>
        <AllowAnonymous>
        Function CreateComment(<Bind(Include:="ArticleId,Body")> comment As Comment) As ActionResult
            Dim article = db.Articles.Find(comment.ArticleId)
            If article Is Nothing Then
                Return New HttpStatusCodeResult(HttpStatusCode.InternalServerError)
            End If

            comment.CreatedOn = Date.Now
            comment.UpdatedOn = Date.Now
            comment.Article = article

            db.Comments.Add(comment)
            db.SaveChanges()

            Return RedirectToAction("Details", New With {.id = comment.ArticleId})
        End Function

        <Authorize(Roles:="Owners")>
        Function DeleteComment(id As Integer?) As ActionResult
            If id Is Nothing Then
                Return New HttpStatusCodeResult(HttpStatusCode.BadRequest)
            End If

            Dim comment = db.Comments.Find(id)
            If comment Is Nothing Then
                Return HttpNotFound()
            End If
            Return View(comment)
        End Function

        <HttpPost, ActionName("DeleteComment")>
        <ValidateAntiForgeryToken>
        <Authorize(Roles:="Owners")>
        Function DeleteCommentConfirmed(id As Integer) As ActionResult
            Dim comment = db.Comments.Find(id)
            Dim articleId As Integer = comment.ArticleId
            db.Comments.Remove(comment)
            db.SaveChanges()

            Return RedirectToAction("Details", New With {.id = articleId})
        End Function

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If (disposing) Then
                db.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

    End Class

End Namespace