﻿@ModelType MyBlogApp.Models.Entities.Category
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>詳細</h2>

<div>
    <h4>カテゴリ</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.CategoryName)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CategoryName)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ArticleCount)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ArticleCount)
        </dd>
    </dl>
</div>
<p>
    @If User.IsInRole("Owners") Then
        @Html.ActionLink("編集", "Edit", New With {.id = Model.CategoryId})
    End If
    @Html.ActionLink("リストへ戻る", "Index")
</p>

<hr />

@For Each item In Model.Articles
    @<div class="panel panel-default">
        <div class="panel-heading">
            @Html.DisplayFor(Function(model) item.Title)
        </div>
        <div class="panel-body">
            <div class="article-body">
                @Html.DisplayFor(Function(model) item.Body)
            </div>
            <div class="text-right text-muted">
                @Html.ActionLink("続きを読む", "Details", New With {.id = item.ArticleId, .Controller = "Articles"})
            </div>
        </div>
        <div class="panel-footer text-right">
            @Html.DisplayNameFor(Function(model) model.CategoryName)
            @Html.ActionLink(item.Category.CategoryName, "Details", New With {.id = item.Category.CategoryId})

            @Html.DisplayNameFor(Function(model) item.CreatedOn)
            @Html.DisplayFor(Function(modelItem) item.CreatedOn)

            @Html.DisplayNameFor(Function(model) item.UpdatedOn)
            @Html.DisplayFor(Function(modelItem) item.UpdatedOn)
        </div>
    </div>
Next
