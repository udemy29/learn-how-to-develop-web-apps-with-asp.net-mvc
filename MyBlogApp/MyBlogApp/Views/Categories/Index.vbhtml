﻿@ModelType IEnumerable(Of MyBlogApp.Models.Entities.Category)
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>カテゴリ一覧</h2>

<p>
    @If User.IsInRole("Owners") Then
        @Html.ActionLink("新規作成", "Create")
    End If
</p>
<table class="table">
    <tr>
        <th>
            @Html.DisplayNameFor(Function(model) model.CategoryName)
        </th>
        <th>
            @Html.DisplayNameFor(Function(model) model.ArticleCount)
        </th>
        <th></th>
    </tr>

    @For Each item In Model
        @<tr>
            <td>
                @Html.DisplayFor(Function(modelItem) item.CategoryName)
            </td>
            <td>
                @Html.DisplayFor(Function(modelItem) item.ArticleCount)
            </td>
            <td>
                @If User.IsInRole("Owners") Then
                    @Html.ActionLink("編集", "Edit", New With {.id = item.CategoryId})
                    @Html.ActionLink("詳細", "Details", New With {.id = item.CategoryId})
                    @If item.ArticleCount = 0 Then
                        @Html.ActionLink("削除", "Delete", New With {.id = item.CategoryId})
                    End If
                End If
            </td>
        </tr>
    Next
</table>