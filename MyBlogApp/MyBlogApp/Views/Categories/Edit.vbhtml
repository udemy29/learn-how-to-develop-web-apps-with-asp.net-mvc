﻿@ModelType MyBlogApp.Models.Entities.Category
@Code
    ViewData("Title") = "Edit"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>編集</h2>

@Using (Html.BeginForm())
    @Html.AntiForgeryToken()

    @<div class="form-horizontal">
        <h4>カテゴリ</h4>
        <hr />
        @Html.ValidationSummary(True, "", New With {.class = "text-danger"})
        @Html.HiddenFor(Function(model) model.CategoryId)

        <div class="form-group">
            @Html.LabelFor(Function(model) model.CategoryName, htmlAttributes:=New With {.class = "control-label col-md-2"})
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.CategoryName, New With {.htmlAttributes = New With {.class = "form-control"}})
                @Html.ValidationMessageFor(Function(model) model.CategoryName, "", New With {.class = "text-danger"})
            </div>
        </div>

        @Html.HiddenFor(Function(model) model.ArticleCount)
        @Html.HiddenFor(Function(model) model.CreatedBy)
        @Html.HiddenFor(Function(model) model.CreatedOn)

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="保存" class="btn btn-default" />
            </div>
        </div>
    </div>
End Using

<div>
    @Html.ActionLink("一覧へ戻る", "Index")
</div>

@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section