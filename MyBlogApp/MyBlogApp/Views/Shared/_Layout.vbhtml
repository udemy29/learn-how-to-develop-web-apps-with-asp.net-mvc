﻿@Imports MyBlogApp.Models.Entities
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - マイ ASP.NET アプリケーション</title>
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")
    <style type="text/css">
        body {
            padding: 70px
        }

        .article-body {
            height: 60px
        }
    </style>
</head>
<body>
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                @Html.ActionLink("アプリケーション名", "Index", "Home", New With {.area = ""}, New With {.class = "navbar-brand"})
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>@Html.ActionLink("ホーム", "Index", "Home")</li>
                    <li>@Html.ActionLink("詳細", "About", "Home")</li>
                    <li>@Html.ActionLink("問い合わせ", "Contact", "Home")</li>
                    <li>@Html.ActionLink("記事", "Index", "Articles")</li>
                    <li>@Html.ActionLink("カテゴリ", "Index", "Categories")</li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    @If Request.IsAuthenticated Then
                        @<li>@Html.ActionLink("ログアウト", "SignOut", "Login")</li>
                    Else
                        @<li>@Html.ActionLink("ログイン", "Index", "Login")</li>
                    End If
                </ul>
            </div>
        </div>
    </div>
    <div Class="container">
        <div Class="col-md-9">
            @RenderBody()
        </div>
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item active">
                    カテゴリ
                </li>
                @code
                    Dim categories = CType(ViewBag.Categories, List(Of Category))
                    For Each item In categories
                        @<li class="list-group-item">
                            <span class="badge">@Html.DisplayFor(Function(model) item.ArticleCount)</span>
                            @Html.ActionLink(item.CategoryName, "Details", New With {.Controller = "Categories", .id = item.CategoryId})
                        </li>
                    Next
                End Code
            </ul>
        </div>
    </div>

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @RenderSection("scripts", required:=False)
</body>
</html>