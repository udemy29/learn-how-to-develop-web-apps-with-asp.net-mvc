﻿@ModelType IEnumerable(Of MyBlogApp.Models.Entities.Article)
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>記事一覧</h2>

<p>
    @If User.IsInRole("Owners") Then
        @Html.ActionLink("新規作成", "Create")
    End If
</p>

@For Each item In Model
    @<div class="panel panel-default">
        <div class="panel-heading">
            @Html.DisplayFor(Function(modelItem) item.Title)
        </div>
        <div class="panel-body">
            <div class="article-body">
                @Html.DisplayFor(Function(modelItem) item.Body)
            </div>
            <div class="text-right text-muted">
                @Html.ActionLink("続きを読む", "Details", New With {.id = item.ArticleId})
            </div>
        </div>
        <div class="panel-footer text-right">
            @Html.DisplayNameFor(Function(model) model.Category.CategoryName)
            @Html.DisplayFor(Function(modelItem) item.Category.CategoryName)（リンク）

            @Html.DisplayNameFor(Function(model) model.CreatedOn)
            @Html.DisplayFor(Function(modelItem) item.CreatedOn)

            @Html.DisplayNameFor(Function(model) model.UpdatedOn)
            @Html.DisplayFor(Function(modelItem) item.UpdatedOn)
        </div>
    </div>
Next