﻿@Imports MyBlogApp.Models.Entities
@ModelType MyBlogApp.Models.Entities.Article
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<div>
    <div class="page-header">
        <h2>@Html.DisplayFor(Function(model) model.Title)</h2>
    </div>
    <p class="text-muted">
        @Html.DisplayNameFor(Function(model) model.Category.CategoryName)
        @Html.DisplayFor(Function(model) model.Category.CategoryName)（リンク）
    </p>
    <div class="lead">
        @Html.DisplayFor(Function(model) model.Body)
    </div>
    <div class="text-right text-muted">
        @Html.DisplayNameFor(Function(model) model.CreatedOn):
        @Html.DisplayFor(Function(model) model.CreatedOn)

        @Html.DisplayNameFor(Function(model) model.UpdatedOn):
        @Html.DisplayFor(Function(model) model.UpdatedOn)

        @If User.IsInRole("Owners") Then
            @Html.ActionLink("編集", "Edit", New With {.id = Model.ArticleId}, New With {.class = "btn btn-xs btn-info"})
            @Html.ActionLink("削除", "Delete", New With {.id = Model.ArticleId}, New With {.class = "btn btn-xs btn-danger"})
        End If
    </div>

    <hr />

    @Using (Html.BeginForm("CreateComment", "Articles"))
        @Html.AntiForgeryToken()
        @Html.Hidden("ArticleId", Model.ArticleId)

        @<div class="form-group">
            @Html.TextArea("Body", "", New With {.class = "form-control", .placeholder = "コメント", .row = 3})
        </div>
        @<input type="submit" value="投稿" class="btn btn-default" />
    End Using

    <hr />

    @For Each item As Comment In Model.Comments

        @<div class="panel panel-default">
            <div class="panel-body">
                @Html.DisplayFor(Function(model) item.Body)
            </div>
            <div class="panel-footer text-right">
                <span class="text-muted">
                    @Html.DisplayNameFor(Function(model) model.CreatedOn)
                    @Html.DisplayFor(Function(model) model.CreatedOn)
                </span>
                @If User.IsInRole("Owners") Then
                    @Html.ActionLink("削除", "DeleteComment", New With {.id = item.CommentId}, New With {.class = "btn btn-xs btn-danger"})
                End If
            </div>
        </div>
    Next
</div>
<p>
            @Html.ActionLink("リストへ戻る", "Index")
</p>
