﻿@ModelType MyBlogApp.Models.Entities.Comment
@Code
    ViewData("Title") = "DeleteComment"
    Layout = "~/Views/Shared/_Layout.vbhtml"
End Code

<h2>DeleteComment</h2>

<h3>このコメントを削除してもよろしいですか？</h3>
<div>
    <h4>コメント</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Article.Title)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Article.Title)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Body)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Body)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.CreatedOn)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.CreatedOn)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
    <input type="submit" value="削除" class="btn btn-danger" /> |
    @Html.ActionLink("記事へ戻る", "Details", New With {.id = Model.ArticleId})
</div>
    End Using
</div>
