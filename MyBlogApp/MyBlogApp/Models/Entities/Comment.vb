Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Models.Entities

    <Table("FREE.Comments")>
    Partial Public Class Comment

        <Key>
        Public Property CommentId As Integer

        <Required>
        <StringLength(500)>
        <DisplayName("コメント")>
        Public Property Body As String

        <DisplayName("投稿者")>
        Public Property CreatedBy As Integer

        <DisplayName("投稿日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

        <DisplayName("本文")>
        Public Property ArticleId As Integer

        <ForeignKey("ArticleId")>
        Public Overridable Property Article As Article

    End Class

End Namespace