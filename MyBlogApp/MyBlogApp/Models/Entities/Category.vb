Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Models.Entities

    <Table("FREE.Categories")>
    Partial Public Class Category

        Public Sub New()
            Articles = New HashSet(Of Article)()
        End Sub

        <Key>
        <DisplayName("カテゴリ")>
        Public Property CategoryId As Integer

        <Required>
        <StringLength(50)>
        <Index(IsUnique:=True)>
        <DisplayName("カテゴリ名")>
        Public Property CategoryName As String

        <DisplayName("件数")>
        <Column("Count")>
        Public Property ArticleCount As Integer

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

        Public Overridable Property Articles As ICollection(Of Article)
    End Class

End Namespace