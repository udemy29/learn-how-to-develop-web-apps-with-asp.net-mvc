﻿Namespace Models.Providers

    Public Class CustomRoleProvider
        Inherits RoleProvider

        Public Overrides Property ApplicationName As String
            Get
                Throw New NotImplementedException()
            End Get
            Set(value As String)
                Throw New NotImplementedException()
            End Set
        End Property

        Public Overrides Sub CreateRole(roleName As String)
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub AddUsersToRoles(usernames() As String, roleNames() As String)
            Throw New NotImplementedException()
        End Sub

        Public Overrides Sub RemoveUsersFromRoles(usernames() As String, roleNames() As String)
            Throw New NotImplementedException()
        End Sub

        Public Overrides Function IsUserInRole(username As String, roleName As String) As Boolean
            Dim roles() As String = GetRolesForUser(username)
            Return roles.Contains(roleName)
        End Function

        Public Overrides Function GetRolesForUser(username As String) As String()
            If "admin".Equals(username) Then
                Return New String() {"Owners"}
            End If
            Return New String() {"Guests"}
        End Function

        Public Overrides Function DeleteRole(roleName As String, throwOnPopulatedRole As Boolean) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Overrides Function RoleExists(roleName As String) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Overrides Function GetUsersInRole(roleName As String) As String()
            Throw New NotImplementedException()
        End Function

        Public Overrides Function GetAllRoles() As String()
            Throw New NotImplementedException()
        End Function

        Public Overrides Function FindUsersInRole(roleName As String, usernameToMatch As String) As String()
            Throw New NotImplementedException()
        End Function

    End Class

End Namespace