Imports System.Data.Entity
Imports MyBlogApp.Models.Entities

Namespace Models

    Partial Public Class FreeContext
        Inherits DbContext

        Public Sub New()
            MyBase.New("name=FreeContext")
            Database.SetInitializer(Of FreeContext)(Nothing)
            Database.Log = Sub(log) Console.WriteLine(log)
        End Sub

        Public Overridable Property Articles As DbSet(Of Article)
        Public Overridable Property Categories As DbSet(Of Category)
        Public Overridable Property Comments As DbSet(Of Comment)

        Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
            modelBuilder.Entity(Of Article)() _
                .HasMany(Function(e) e.Comments) _
                .WithRequired(Function(e) e.Article) _
                .WillCascadeOnDelete(False)

            modelBuilder.Entity(Of Category)() _
                .HasMany(Function(e) e.Articles) _
                .WithRequired(Function(e) e.Category) _
                .WillCascadeOnDelete(False)
        End Sub

    End Class

End Namespace