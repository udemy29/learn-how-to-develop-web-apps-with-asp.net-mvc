﻿Imports System.Web.Mvc

<TestClass()> Public Class HomeControllerTest
    Private ReadOnly controller As New HomeController()

    <TestMethod()> Public Sub Index()
        ' Act
        Dim result As ViewResult = DirectCast(controller.Index(), ViewResult)

        ' Assert
        Assert.IsNotNull(result)
    End Sub

    <TestMethod()> Public Sub About()
        ' Act
        Dim result As ViewResult = DirectCast(controller.About(), ViewResult)

        ' Assert
        Dim viewData As ViewDataDictionary = result.ViewData
        Assert.AreEqual("Your application description page.", viewData("Message"))
    End Sub

    <TestMethod()> Public Sub Contact()
        ' Act
        Dim result As ViewResult = DirectCast(controller.Contact(), ViewResult)

        ' Assert
        Assert.IsNotNull(result)
    End Sub

End Class